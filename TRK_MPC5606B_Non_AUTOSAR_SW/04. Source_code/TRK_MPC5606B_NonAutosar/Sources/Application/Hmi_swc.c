/*
 * Hmi_swc.c
 *
 *  Created on: May 21, 2018
 *      Author: Windows
 */

#include "FT8.h"
#include "FT8_commands.h"
#include "string.h"
#include "dspi_var.h"
#include "Hmi_swc.h"

void TFT_init(void);
static void Init_SpeedoMeter(void);
/*global Variables*/

volatile int speed=0;
volatile int odo=12;
volatile int Hor=5;
volatile int min=45;
volatile int sec=55;


/*this function init the GUI module and Display the welcome Screen for 5 sec*/
void Init_GUI_Module(void)
{
	uint32_t Delay_Sec;
	/*Init SPIbase 0 wich is connected with the Display*/
	//initDSPI_0();
	/*Init Display by sending Init CMD to the display*/
	TFT_init();
	/*Welcome Screen Display*/
	/*FT8_cmd_dl(CMD_DLSTART);
	FT8_cmd_dl(DL_CLEAR);
	FT8_cmd_dl(DL_CLEAR_RGB | BLACK);
	FT8_cmd_dl(DL_CLEAR | CLR_COL | CLR_STN | CLR_TAG);
	FT8_cmd_dl(DL_COLOR_RGB | ORANGE);
	FT8_cmd_text((FT8_HSIZE/2), (FT8_VSIZE/2), 30, FT8_OPT_CENTER, "NIYATA INFOTECH");
	FT8_cmd_dl(DL_COLOR_RGB | WHITE);
	FT8_cmd_text(300,260,29,0,"Connecting Dots...");
	FT8_cmd_dl(DL_DISPLAY);
	FT8_cmd_dl(CMD_SWAP);
	FT8_cmd_execute();
	for(Delay_Sec=0;Delay_Sec<9000000;Delay_Sec++);
	for(Delay_Sec=0;Delay_Sec<9000000;Delay_Sec++);*/
	Init_SpeedoMeter();
	for(Delay_Sec=0;Delay_Sec<100000;Delay_Sec++)
	{
		
	}
}

/*This function is used to unit the gauge for display init */
static void Init_SpeedoMeter(void)
{
	uint8_t test;
	
	for(test=0;test<160;test++)
	{
	FT8_cmd_dl(CMD_DLSTART);
	FT8_cmd_dl(DL_CLEAR);
	FT8_cmd_dl(DL_CLEAR_RGB | BLACK);
	FT8_cmd_dl(DL_CLEAR | CLR_COL | CLR_STN | CLR_TAG);
	FT8_cmd_dl(DL_COLOR_RGB | ORANGE);
	FT8_cmd_gauge(236+150, 159, 140, 1, 9, 4, (uint16_t)test, 160);
	//FT8_cmd_dl(DL_COLOR_RGB | ORANGE);
	FT8_cmd_text((FT8_HSIZE/2), (360), 30, FT8_OPT_CENTER, "NIYATA INFOTECH");
	FT8_cmd_dl(DL_COLOR_RGB | WHITE);
	FT8_cmd_text(300,380,29,0,"Connecting Dots...");
	FT8_cmd_dl(DL_DISPLAY);
	FT8_cmd_dl(CMD_SWAP);
	FT8_cmd_execute();
	}
	for(test=160;test>0;test--)
	{
	FT8_cmd_dl(CMD_DLSTART);
	FT8_cmd_dl(DL_CLEAR);
	FT8_cmd_dl(DL_CLEAR_RGB | BLACK);
	FT8_cmd_dl(DL_CLEAR | CLR_COL | CLR_STN | CLR_TAG);
	FT8_cmd_dl(DL_COLOR_RGB | ORANGE);
	FT8_cmd_gauge(236+150, 159, 140, 1, 9, 4, (uint16_t)test, 160);
	//FT8_cmd_dl(DL_COLOR_RGB | ORANGE);
	FT8_cmd_text((FT8_HSIZE/2), (360), 30, FT8_OPT_CENTER, "NIYATA INFOTECH");
	FT8_cmd_dl(DL_COLOR_RGB | WHITE);
	FT8_cmd_text(300,380,29,0,"Connecting Dots....");
	FT8_cmd_dl(DL_DISPLAY);
	FT8_cmd_dl(CMD_SWAP);
	FT8_cmd_execute();
	}
}

/*This funtion is used to update the latest speed,ODO,time in the display this function should be called for every 100ms*/
void Update_GUI_Display_Loop(void)
{
	FT8_cmd_dl(CMD_DLSTART);
	FT8_cmd_dl(DL_CLEAR);
	FT8_cmd_dl(DL_CLEAR_RGB | WHITE);
	FT8_cmd_dl(DL_CLEAR | CLR_COL | CLR_STN | CLR_TAG);
	if(speed<60)
	{
	FT8_cmd_dl(DL_COLOR_RGB | GREEN);
	}
	else if(speed > 60 && speed <110)
	{
	FT8_cmd_dl(DL_COLOR_RGB | YELLOW);
	}
	else
	{
	FT8_cmd_dl(DL_COLOR_RGB | RED);
	}
	FT8_cmd_gauge(236+150, 159, 140, 1, 9, 4, (uint16_t)speed, 160);
	FT8_cmd_dl(DL_COLOR_RGB | WHITE);
	FT8_cmd_text(206+155, 212, 28, 0, "   Kmph");
	FT8_cmd_number(185+150, 212, 28,3,speed);//text speed value
	FT8_cmd_number(190+150, 250, 29,6,odo);//odo value
	FT8_cmd_dl(DL_COLOR_RGB | BLACK);
	FT8_cmd_number(200+150, 350, 28,2,Hor);//hr
	FT8_cmd_text(200+175, 350, 28, 0, ":");
	FT8_cmd_number(200+180, 350, 28,2,min);//min
	FT8_cmd_text(200+205, 350, 28, 0, ":");
	FT8_cmd_number(200+210, 350, 28,2,sec);//sec
	FT8_cmd_text(200+150, 380, 27,0,"HH:MM:SS");//hr
	FT8_cmd_dl(DL_DISPLAY);
	FT8_cmd_dl(CMD_SWAP);
	FT8_cmd_execute();
}

void Update_GUI_Time(uint8_t Hour,uint8_t Min,uint8_t Sec)
{
	Hor=Hour;
	min=Min;
	sec=Sec;
	
}

void Update_GUI_Speedo(uint8_t Speed)
{
	speed=Speed;
}

void Update_GUI_Odometer(uint8_t Odo_meter)
{
	odo=Odo_meter;
}
void TFT_init(void)
{
	//uint32_t touch_a, touch_b, touch_c, touch_d, touch_e, touch_f;
	if(FT8_init() != 0)
	{
		
		FT8_memWrite8(REG_PWM_DUTY, 40);	/* reduce the current necessary for the backlight -> weak psu.. */

		/* send pre-recorded touch calibration values, HY50HD */
		FT8_memWrite32(REG_TOUCH_TRANSFORM_A, 57513);
		FT8_memWrite32(REG_TOUCH_TRANSFORM_B, 159);
		FT8_memWrite32(REG_TOUCH_TRANSFORM_C, 4291830562);
		FT8_memWrite32(REG_TOUCH_TRANSFORM_D, 4294964932);
		FT8_memWrite32(REG_TOUCH_TRANSFORM_E, 4294932109);
		FT8_memWrite32(REG_TOUCH_TRANSFORM_F, 34415806);

#if 0
 		/* calibrate touch and displays values to screen */
		FT8_cmd_dl(CMD_DLSTART);
		FT8_cmd_dl(DL_CLEAR_RGB | BLACK);
		FT8_cmd_dl(DL_CLEAR | CLR_COL | CLR_STN | CLR_TAG);
		FT8_cmd_text((FT8_HSIZE/2), (FT8_VSIZE/2), 27, FT8_OPT_CENTER, "Please tap on the dot.");
		FT8_cmd_calibrate();
		FT8_cmd_dl(DL_DISPLAY);
		FT8_cmd_dl(CMD_SWAP);
		FT8_cmd_execute();

		

		touch_a = FT8_memRead32(REG_TOUCH_TRANSFORM_A);
		touch_b = FT8_memRead32(REG_TOUCH_TRANSFORM_B);
		touch_c = FT8_memRead32(REG_TOUCH_TRANSFORM_C);
		touch_d = FT8_memRead32(REG_TOUCH_TRANSFORM_D);
		touch_e = FT8_memRead32(REG_TOUCH_TRANSFORM_E);
		touch_f = FT8_memRead32(REG_TOUCH_TRANSFORM_F);

		FT8_cmd_dl(CMD_DLSTART);
		FT8_cmd_dl(DL_CLEAR_RGB | BLACK);
		FT8_cmd_dl(DL_CLEAR | CLR_COL | CLR_STN | CLR_TAG);
		FT8_cmd_dl(TAG(0));

		FT8_cmd_text(5, 30, 28, 0, "TOUCH_TRANSFORM_A:");
		FT8_cmd_text(5, 50, 28, 0, "TOUCH_TRANSFORM_B:");
		FT8_cmd_text(5, 70, 28, 0, "TOUCH_TRANSFORM_C:");
		FT8_cmd_text(5, 90, 28, 0, "TOUCH_TRANSFORM_D:");
		FT8_cmd_text(5, 110, 28, 0, "TOUCH_TRANSFORM_E:");
		FT8_cmd_text(5, 130, 28, 0, "TOUCH_TRANSFORM_F:");

#ifdef FT8_81X_ENABLE
//		FT8_cmd_setbase(16L); /* FT81x only */
#endif
		FT8_cmd_number(250, 30, 28, 0, touch_a);
		FT8_cmd_number(250, 50, 28, 0, touch_b);
		FT8_cmd_number(250, 70, 28, 0, touch_c);
		FT8_cmd_number(250, 90, 28, 0, touch_d);
		FT8_cmd_number(250, 110, 28, 0, touch_e);
		FT8_cmd_number(250, 130, 28, 0, touch_f);

		FT8_cmd_dl(DL_DISPLAY);	/* instruct the graphics processor to show the list */
		FT8_cmd_dl(CMD_SWAP); /* make this list active */
		FT8_cmd_execute();
		
		while(1);
#endif

	}
}

