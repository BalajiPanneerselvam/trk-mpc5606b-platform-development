/*
 * Hmi_swc.h
 *
 *  Created on: May 21, 2018
 *      Author: Windows
 */

#ifndef HMI_SWC_H_
#define HMI_SWC_H_

void Init_GUI_Module(void);
void Update_GUI_Display_Loop(void);
void Update_GUI_Time(uint8_t Hour,uint8_t Min,uint8_t Sec);

void Update_GUI_Speedo(uint8_t Speed);
void Update_GUI_Odometer(uint8_t Odo_meter);

#endif /* HMI_SWC_H_ */
