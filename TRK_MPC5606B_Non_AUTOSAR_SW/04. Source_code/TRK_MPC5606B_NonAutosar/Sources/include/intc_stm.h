/*
 *######################################################################
 *                                RAppIDJDP
 *           Rapid Application Initialization and Documentation Tool
 *                         Freescale Semiconductor Inc.
 *
 *######################################################################
 *
 * Project Name           : TRK_MPC_5606B_NonAutosar
 *
 * Project File           : TRK_MPC_5606B_NonAutosar.rsp
 *
 * Revision Number        : 1.0
 *
 * Tool Version           : 1.2.1.5
 *
 * file                   : intc_stm.h
 *
 * Target Compiler        : Codewarrior
 *
 * Target Part            : MPC5606B
 *
 * Part Errata Fixes      : none
 *
 * Project Last Save Date : 19-May-2018 18:45:05
 *
 * Created on Date        : 19-May-2018 18:45:06
 *
 * Brief Description      : This  file contains  the interrupt service routine  for the STM
 *
 *
 *######################################################################
*/

#ifndef  _INTC_STM_H
#define  _INTC_STM_H
/********************  Dependent Include files here **********************/

#include "jdp.h"

/**********************  Function Prototype here *************************/

void STM_CH0_ISR(void);


#endif  /*_INTC_STM_H*/

/*
 *######################################################################
 *                           End of File
 *######################################################################
*/

