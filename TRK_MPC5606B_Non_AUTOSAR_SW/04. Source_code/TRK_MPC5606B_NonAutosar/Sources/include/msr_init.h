/*
 *######################################################################
 *                                RAppIDJDP
 *           Rapid Application Initialization and Documentation Tool
 *                         Freescale Semiconductor Inc.
 *
 *######################################################################
 *
 * Project Name           : TRK_MPC_5606B_NonAutosar
 *
 * Project File           : TRK_MPC_5606B_NonAutosar.rsp
 *
 * Revision Number        : 1.0
 *
 * Tool Version           : 1.2.1.5
 *
 * file                   : msr_init.h
 *
 * Target Compiler        : Codewarrior
 *
 * Target Part            : MPC5606B
 *
 * Part Errata Fixes      : none
 *
 * Project Last Save Date : 19-May-2018 18:45:05
 *
 * Created on Date        : 19-May-2018 18:45:06
 *
 * Brief Description      : This File contain function declaration for SPR code file
 *
 *
 *######################################################################
*/

#ifndef  _MSR_INIT_H
#define  _MSR_INIT_H
/**********************  Function Prototype here *************************/

asm void msr_init_fnc (void);


#endif  /*_MSR_INIT_H*/

/*
 *######################################################################
 *                           End of File
 *######################################################################
*/

