/*
 *######################################################################
 *                                RAppIDJDP
 *           Rapid Application Initialization and Documentation Tool
 *                         Freescale Semiconductor Inc.
 *
 *######################################################################
 *
 * Project Name           : TRK_MPC_5606B_NonAutosar
 *
 * Project File           : TRK_MPC_5606B_NonAutosar.rsp
 *
 * Revision Number        : 1.0
 *
 * Tool Version           : 1.2.1.5
 *
 * file                   : dspi_var.c
 *
 * Target Compiler        : Codewarrior
 *
 * Target Part            : MPC5606B
 *
 * Part Errata Fixes      : none
 *
 * Project Last Save Date : 19-May-2018 18:45:05
 *
 * Created on Date        : 19-May-2018 18:45:06
 *
 * Brief Description      : This File contains variable definition for Transmit
 *                          and Receive queues
 *
 ******************************************************************************** 
 *
 * Detail Description     : This File contains variables which contain Transmission
 *                         and reception data for respective DSPI module.
 *
 ******************************************************************************** 
 *
 *######################################################################
*/

 
 
/********************  Dependent Include files here **********************/

#include "typedefs.h"

#include "dspi_var.h"

vuint32_t DSPI0_TxQUE[1]= {0U};    /* Transmit Queue DSPI_0 Variable */
vuint32_t DSPI1_TxQUE[1]= {0U};    /* Transmit Queue DSPI_1 Variable */
vuint32_t DSPI2_TxQUE[1]= {0U};    /* Transmit Queue DSPI_2 Variable */
vuint32_t DSPI3_TxQUE[1]= {0U};    /* Transmit Queue DSPI_3 Variable */
vuint32_t DSPI4_TxQUE[1]= {0U};    /* Transmit Queue DSPI_4 Variable */

vuint16_t DSPI0_RxQUE[1]= {0U };    /* Receive Queue DSPI_0 Variable */
vuint16_t DSPI1_RxQUE[1]= {0U };    /* Receive Queue DSPI_1 Variable */
vuint16_t DSPI2_RxQUE[1]= {0U };    /* Receive Queue DSPI_2 Variable */
vuint16_t DSPI3_RxQUE[1]= {0U };    /* Receive Queue DSPI_3 Variable */
vuint16_t DSPI4_RxQUE[1]= {0U };    /* Receive Queue DSPI_4 Variable */


void FT8_cs_set(void)
{
	/*Set CS pin Low */
	SIU.GPDO[15].B.PDO = 0;
	
}

void FT8_cs_clear(void)
{
	/*Set CS pin High */
	SIU.GPDO[15].B.PDO = 1;	
		
}

void FT8_pdn_set(void)
{
	/*Power Down pin Low*/
	SIU.GPDO[11].B.PDO = 0;
	
}

void FT8_pdn_clear(void)
{
	
	/*Power Down pin Low*/
    SIU.GPDO[11].B.PDO = 1;
	

}

uint8_t spi_transmit(uint8_t data)
{
	uint8_t rx_data;
	/* Write into tx-fifo */
	DSPI_0.PUSHR.R = ((uint32_t)(uint32_t)0x08000000 |(uint32_t)data);
	/* Wait until transmit success */
	while (DSPI_0.SR.B.TCF != TRANSMIT_COMPLETE){
		;
	}
	while (DSPI_0.SR.B.RFDF != RECEIVED_DATA){
		;  /* Wait for Receive FIFO Drain Flag = 1 */
	}
	rx_data = (uint8_t)DSPI_0.POPR.R;   /* Read data received by master SPI */
	DSPI_0.SR.R = 0x90020000;        /* Clear TCF, RDRF, EOQ flags by writing 1 */	
	
	return rx_data;
}
 
/*
 *######################################################################
 *                           End of File
 *######################################################################
*/

