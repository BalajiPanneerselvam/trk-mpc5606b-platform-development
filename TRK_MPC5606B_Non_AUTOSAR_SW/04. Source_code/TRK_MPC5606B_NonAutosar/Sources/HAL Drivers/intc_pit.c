/*
 *######################################################################
 *                                RAppIDJDP
 *           Rapid Application Initialization and Documentation Tool
 *                         Freescale Semiconductor Inc.
 *
 *######################################################################
 *
 * Project Name           : TRK_MPC_5606B_NonAutosar
 *
 * Project File           : TRK_MPC_5606B_NonAutosar.rsp
 *
 * Revision Number        : 1.0
 *
 * Tool Version           : 1.2.1.5
 *
 * file                   : intc_pit.c
 *
 * Target Compiler        : Codewarrior
 *
 * Target Part            : MPC5606B
 *
 * Part Errata Fixes      : none
 *
 * Project Last Save Date : 19-May-2018 18:45:05
 *
 * Created on Date        : 19-May-2018 18:45:06
 *
 * Brief Description      : This  file contains  the interrupt service routine  for the Periodic Interrupt Timer
 *
 ******************************************************************************** 
 *
 * Detail Description     : This file is generated when PIT(Periodic Interrupt
 *                         Timer) function is defined in INTC peripheral.This
 *                         file contains the Interrupt handlers routines for PIT.
 *                         In Interrupt handlers routine respective flags are cleared.
 *
 ******************************************************************************** 
 *
 *######################################################################
*/

 
 
/********************  Dependent Include files here **********************/

#include "intc_pit.h"


/************************* INTERRUPT HANDLERS ************************/

void PIT_CH0_ISR (void)
{
    PIT.CH[0].TFLG.R = 0x00000001;

}


void PIT_CH1_ISR (void)
{
    PIT.CH[1].TFLG.R = 0x00000001;

}


 
/*
 *######################################################################
 *                           End of File
 *######################################################################
*/

