/*
 *######################################################################
 *                                RAppIDJDP
 *           Rapid Application Initialization and Documentation Tool
 *                         Freescale Semiconductor Inc.
 *
 *######################################################################
 *
 * Project Name           : TRK_MPC_5606B_NonAutosar
 *
 * Project File           : TRK_MPC_5606B_NonAutosar.rsp
 *
 * Revision Number        : 1.0
 *
 * Tool Version           : 1.2.1.5
 *
 * file                   : intc_stm.c
 *
 * Target Compiler        : Codewarrior
 *
 * Target Part            : MPC5606B
 *
 * Part Errata Fixes      : none
 *
 * Project Last Save Date : 19-May-2018 18:45:05
 *
 * Created on Date        : 19-May-2018 18:45:06
 *
 * Brief Description      : This  file contains  the interrupt service routine  for the STM
 *
 ******************************************************************************** 
 *
 * Detail Description     : This file is generated when STM function is defined
 *                         in INTC peripheral.This file contains the Interrupt
 *                         handlers routines for STM. In Interrupt handlers
 *                         routine respective flags are cleared.
 *
 ******************************************************************************** 
 *
 *######################################################################
*/

 
 
/********************  Dependent Include files here **********************/

#include "intc_stm.h"


/************************* INTERRUPT HANDLERS ************************/

void STM_CH0_ISR (void)
{
    STM.CIR0.R = 0x00000001;

    STM.CR0.B.TEN = 0x0;
    STM.CNT0.R = 0x00000000;
    STM.CR0.B.TEN = 0x1;
        /* Clear STM Counter Register */

}


 
/*
 *######################################################################
 *                           End of File
 *######################################################################
*/

