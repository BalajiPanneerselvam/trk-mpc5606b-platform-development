/*
 *######################################################################
 *                                RAppIDJDP
 *           Rapid Application Initialization and Documentation Tool
 *                         Freescale Semiconductor Inc.
 *
 *######################################################################
 *
 * Project Name           : TRK_MPC_5606B_NonAutosar
 *
 * Project File           : TRK_MPC_5606B_NonAutosar.rsp
 *
 * Revision Number        : 1.0
 *
 * Tool Version           : 1.2.1.5
 *
 * file                   : main.c
 *
 * Target Compiler        : Codewarrior
 *
 * Target Part            : MPC5606B
 *
 * Part Errata Fixes      : none
 *
 * Project Last Save Date : 19-May-2018 18:45:05
 *
 * Created on Date        : 19-May-2018 18:45:06
 *
 * Brief Description      : This file contains main() function call.
 *
 ******************************************************************************** 
 *
 * Detail Description     : This file contains main() routine which calls system
 *                         initialization routine and interrupt enable routine if defined.
 *
 ******************************************************************************** 
 *
 *######################################################################
*/

 
 
/********************  Dependent Include files here **********************/

#include "rappid_ref.h"
#include "rappid_utils.h"
#include "sys_init.h"
#include "Hmi_swc.h"
/**********************  Function Prototype here *************************/

void main(void);


/*********************  Initialization Function(s) ************************/

void main(void)
{
	long int count;
/* ----------------------------------------------------------- */
/*	             System Initialization Function                  */
/* ----------------------------------------------------------- */
   sys_init_fnc();
/********* Enable External Interrupt *********/
   EnableExternalInterrupts();
   
   //Init_GUI_Module();           /*InitGUI module and related pheripherals*/
   while(1)
   {
	 for(count=0;count<1000000;count++)
	 {
		 
		 ;
		 
	 }
	 
	 SIU.GPDO[68].B.PDO ^= 1; 
	   
   }

}

 
/*
 *######################################################################
 *                           End of File
 *######################################################################
*/

