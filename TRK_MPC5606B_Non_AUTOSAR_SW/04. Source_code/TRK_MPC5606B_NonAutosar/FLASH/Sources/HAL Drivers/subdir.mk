################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/HAL Drivers/Uart_swc.c" \
"../Sources/HAL Drivers/dspi_init.c" \
"../Sources/HAL Drivers/dspi_var.c" \
"../Sources/HAL Drivers/emios_init.c" \
"../Sources/HAL Drivers/flash_init.c" \
"../Sources/HAL Drivers/flexcan_init.c" \
"../Sources/HAL Drivers/intc_init.c" \
"../Sources/HAL Drivers/intc_pit.c" \
"../Sources/HAL Drivers/intc_stm.c" \
"../Sources/HAL Drivers/intc_sw_vecttable.c" \
"../Sources/HAL Drivers/main.c" \
"../Sources/HAL Drivers/msr_init.c" \
"../Sources/HAL Drivers/pcu_init.c" \
"../Sources/HAL Drivers/pit_init.c" \
"../Sources/HAL Drivers/rappid_utils.c" \
"../Sources/HAL Drivers/rchw_init.c" \
"../Sources/HAL Drivers/rgm_init.c" \
"../Sources/HAL Drivers/romcopy.c" \
"../Sources/HAL Drivers/rtc_init.c" \
"../Sources/HAL Drivers/siu_init.c" \
"../Sources/HAL Drivers/stm_init.c" \
"../Sources/HAL Drivers/swt_init.c" \
"../Sources/HAL Drivers/sys_init.c" \
"../Sources/HAL Drivers/sysclk_init.c" \

C_SRCS += \
../Sources/HAL\ Drivers/Uart_swc.c \
../Sources/HAL\ Drivers/dspi_init.c \
../Sources/HAL\ Drivers/dspi_var.c \
../Sources/HAL\ Drivers/emios_init.c \
../Sources/HAL\ Drivers/flash_init.c \
../Sources/HAL\ Drivers/flexcan_init.c \
../Sources/HAL\ Drivers/intc_init.c \
../Sources/HAL\ Drivers/intc_pit.c \
../Sources/HAL\ Drivers/intc_stm.c \
../Sources/HAL\ Drivers/intc_sw_vecttable.c \
../Sources/HAL\ Drivers/main.c \
../Sources/HAL\ Drivers/msr_init.c \
../Sources/HAL\ Drivers/pcu_init.c \
../Sources/HAL\ Drivers/pit_init.c \
../Sources/HAL\ Drivers/rappid_utils.c \
../Sources/HAL\ Drivers/rchw_init.c \
../Sources/HAL\ Drivers/rgm_init.c \
../Sources/HAL\ Drivers/romcopy.c \
../Sources/HAL\ Drivers/rtc_init.c \
../Sources/HAL\ Drivers/siu_init.c \
../Sources/HAL\ Drivers/stm_init.c \
../Sources/HAL\ Drivers/swt_init.c \
../Sources/HAL\ Drivers/sys_init.c \
../Sources/HAL\ Drivers/sysclk_init.c \

S_SRCS += \
../Sources/HAL\ Drivers/crt0.s \
../Sources/HAL\ Drivers/excep_handler.s \
../Sources/HAL\ Drivers/rappid_func.s \
../Sources/HAL\ Drivers/sw_handlers.s \

S_SRCS_QUOTED += \
"../Sources/HAL Drivers/crt0.s" \
"../Sources/HAL Drivers/excep_handler.s" \
"../Sources/HAL Drivers/rappid_func.s" \
"../Sources/HAL Drivers/sw_handlers.s" \

S_DEPS_QUOTED += \
"./Sources/HAL Drivers/crt0_s.d" \
"./Sources/HAL Drivers/excep_handler_s.d" \
"./Sources/HAL Drivers/rappid_func_s.d" \
"./Sources/HAL Drivers/sw_handlers_s.d" \

OBJS += \
./Sources/HAL\ Drivers/Uart_swc_c.obj \
./Sources/HAL\ Drivers/crt0_s.obj \
./Sources/HAL\ Drivers/dspi_init_c.obj \
./Sources/HAL\ Drivers/dspi_var_c.obj \
./Sources/HAL\ Drivers/emios_init_c.obj \
./Sources/HAL\ Drivers/excep_handler_s.obj \
./Sources/HAL\ Drivers/flash_init_c.obj \
./Sources/HAL\ Drivers/flexcan_init_c.obj \
./Sources/HAL\ Drivers/intc_init_c.obj \
./Sources/HAL\ Drivers/intc_pit_c.obj \
./Sources/HAL\ Drivers/intc_stm_c.obj \
./Sources/HAL\ Drivers/intc_sw_vecttable_c.obj \
./Sources/HAL\ Drivers/main_c.obj \
./Sources/HAL\ Drivers/msr_init_c.obj \
./Sources/HAL\ Drivers/pcu_init_c.obj \
./Sources/HAL\ Drivers/pit_init_c.obj \
./Sources/HAL\ Drivers/rappid_func_s.obj \
./Sources/HAL\ Drivers/rappid_utils_c.obj \
./Sources/HAL\ Drivers/rchw_init_c.obj \
./Sources/HAL\ Drivers/rgm_init_c.obj \
./Sources/HAL\ Drivers/romcopy_c.obj \
./Sources/HAL\ Drivers/rtc_init_c.obj \
./Sources/HAL\ Drivers/siu_init_c.obj \
./Sources/HAL\ Drivers/stm_init_c.obj \
./Sources/HAL\ Drivers/sw_handlers_s.obj \
./Sources/HAL\ Drivers/swt_init_c.obj \
./Sources/HAL\ Drivers/sys_init_c.obj \
./Sources/HAL\ Drivers/sysclk_init_c.obj \

S_DEPS += \
./Sources/HAL\ Drivers/crt0_s.d \
./Sources/HAL\ Drivers/excep_handler_s.d \
./Sources/HAL\ Drivers/rappid_func_s.d \
./Sources/HAL\ Drivers/sw_handlers_s.d \

OBJS_QUOTED += \
"./Sources/HAL Drivers/Uart_swc_c.obj" \
"./Sources/HAL Drivers/crt0_s.obj" \
"./Sources/HAL Drivers/dspi_init_c.obj" \
"./Sources/HAL Drivers/dspi_var_c.obj" \
"./Sources/HAL Drivers/emios_init_c.obj" \
"./Sources/HAL Drivers/excep_handler_s.obj" \
"./Sources/HAL Drivers/flash_init_c.obj" \
"./Sources/HAL Drivers/flexcan_init_c.obj" \
"./Sources/HAL Drivers/intc_init_c.obj" \
"./Sources/HAL Drivers/intc_pit_c.obj" \
"./Sources/HAL Drivers/intc_stm_c.obj" \
"./Sources/HAL Drivers/intc_sw_vecttable_c.obj" \
"./Sources/HAL Drivers/main_c.obj" \
"./Sources/HAL Drivers/msr_init_c.obj" \
"./Sources/HAL Drivers/pcu_init_c.obj" \
"./Sources/HAL Drivers/pit_init_c.obj" \
"./Sources/HAL Drivers/rappid_func_s.obj" \
"./Sources/HAL Drivers/rappid_utils_c.obj" \
"./Sources/HAL Drivers/rchw_init_c.obj" \
"./Sources/HAL Drivers/rgm_init_c.obj" \
"./Sources/HAL Drivers/romcopy_c.obj" \
"./Sources/HAL Drivers/rtc_init_c.obj" \
"./Sources/HAL Drivers/siu_init_c.obj" \
"./Sources/HAL Drivers/stm_init_c.obj" \
"./Sources/HAL Drivers/sw_handlers_s.obj" \
"./Sources/HAL Drivers/swt_init_c.obj" \
"./Sources/HAL Drivers/sys_init_c.obj" \
"./Sources/HAL Drivers/sysclk_init_c.obj" \

C_DEPS += \
./Sources/HAL\ Drivers/Uart_swc_c.d \
./Sources/HAL\ Drivers/dspi_init_c.d \
./Sources/HAL\ Drivers/dspi_var_c.d \
./Sources/HAL\ Drivers/emios_init_c.d \
./Sources/HAL\ Drivers/flash_init_c.d \
./Sources/HAL\ Drivers/flexcan_init_c.d \
./Sources/HAL\ Drivers/intc_init_c.d \
./Sources/HAL\ Drivers/intc_pit_c.d \
./Sources/HAL\ Drivers/intc_stm_c.d \
./Sources/HAL\ Drivers/intc_sw_vecttable_c.d \
./Sources/HAL\ Drivers/main_c.d \
./Sources/HAL\ Drivers/msr_init_c.d \
./Sources/HAL\ Drivers/pcu_init_c.d \
./Sources/HAL\ Drivers/pit_init_c.d \
./Sources/HAL\ Drivers/rappid_utils_c.d \
./Sources/HAL\ Drivers/rchw_init_c.d \
./Sources/HAL\ Drivers/rgm_init_c.d \
./Sources/HAL\ Drivers/romcopy_c.d \
./Sources/HAL\ Drivers/rtc_init_c.d \
./Sources/HAL\ Drivers/siu_init_c.d \
./Sources/HAL\ Drivers/stm_init_c.d \
./Sources/HAL\ Drivers/swt_init_c.d \
./Sources/HAL\ Drivers/sys_init_c.d \
./Sources/HAL\ Drivers/sysclk_init_c.d \

OBJS_OS_FORMAT += \
./Sources/HAL\ Drivers/Uart_swc_c.obj \
./Sources/HAL\ Drivers/crt0_s.obj \
./Sources/HAL\ Drivers/dspi_init_c.obj \
./Sources/HAL\ Drivers/dspi_var_c.obj \
./Sources/HAL\ Drivers/emios_init_c.obj \
./Sources/HAL\ Drivers/excep_handler_s.obj \
./Sources/HAL\ Drivers/flash_init_c.obj \
./Sources/HAL\ Drivers/flexcan_init_c.obj \
./Sources/HAL\ Drivers/intc_init_c.obj \
./Sources/HAL\ Drivers/intc_pit_c.obj \
./Sources/HAL\ Drivers/intc_stm_c.obj \
./Sources/HAL\ Drivers/intc_sw_vecttable_c.obj \
./Sources/HAL\ Drivers/main_c.obj \
./Sources/HAL\ Drivers/msr_init_c.obj \
./Sources/HAL\ Drivers/pcu_init_c.obj \
./Sources/HAL\ Drivers/pit_init_c.obj \
./Sources/HAL\ Drivers/rappid_func_s.obj \
./Sources/HAL\ Drivers/rappid_utils_c.obj \
./Sources/HAL\ Drivers/rchw_init_c.obj \
./Sources/HAL\ Drivers/rgm_init_c.obj \
./Sources/HAL\ Drivers/romcopy_c.obj \
./Sources/HAL\ Drivers/rtc_init_c.obj \
./Sources/HAL\ Drivers/siu_init_c.obj \
./Sources/HAL\ Drivers/stm_init_c.obj \
./Sources/HAL\ Drivers/sw_handlers_s.obj \
./Sources/HAL\ Drivers/swt_init_c.obj \
./Sources/HAL\ Drivers/sys_init_c.obj \
./Sources/HAL\ Drivers/sysclk_init_c.obj \

C_DEPS_QUOTED += \
"./Sources/HAL Drivers/Uart_swc_c.d" \
"./Sources/HAL Drivers/dspi_init_c.d" \
"./Sources/HAL Drivers/dspi_var_c.d" \
"./Sources/HAL Drivers/emios_init_c.d" \
"./Sources/HAL Drivers/flash_init_c.d" \
"./Sources/HAL Drivers/flexcan_init_c.d" \
"./Sources/HAL Drivers/intc_init_c.d" \
"./Sources/HAL Drivers/intc_pit_c.d" \
"./Sources/HAL Drivers/intc_stm_c.d" \
"./Sources/HAL Drivers/intc_sw_vecttable_c.d" \
"./Sources/HAL Drivers/main_c.d" \
"./Sources/HAL Drivers/msr_init_c.d" \
"./Sources/HAL Drivers/pcu_init_c.d" \
"./Sources/HAL Drivers/pit_init_c.d" \
"./Sources/HAL Drivers/rappid_utils_c.d" \
"./Sources/HAL Drivers/rchw_init_c.d" \
"./Sources/HAL Drivers/rgm_init_c.d" \
"./Sources/HAL Drivers/romcopy_c.d" \
"./Sources/HAL Drivers/rtc_init_c.d" \
"./Sources/HAL Drivers/siu_init_c.d" \
"./Sources/HAL Drivers/stm_init_c.d" \
"./Sources/HAL Drivers/swt_init_c.d" \
"./Sources/HAL Drivers/sys_init_c.d" \
"./Sources/HAL Drivers/sysclk_init_c.d" \


# Each subdirectory must supply rules for building sources it contributes
Sources/HAL\ Drivers/Uart_swc_c.obj: ../Sources/HAL\ Drivers/Uart_swc.c
	@echo 'Building file: $<'
	@echo 'Executing target #2 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/Uart_swc.args" -o "Sources/HAL Drivers/Uart_swc_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/Uart_swc_c.d: ../Sources/HAL\ Drivers/Uart_swc.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/crt0_s.obj: ../Sources/HAL\ Drivers/crt0.s
	@echo 'Building file: $<'
	@echo 'Executing target #3 $<'
	@echo 'Invoking: PowerPC Assembler'
	"$(PAToolsDirEnv)/mwasmeppc" @@"Sources/HAL Drivers/crt0.args" -o "Sources/HAL Drivers/crt0_s.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/crt0_s.d: ../Sources/HAL\ Drivers/crt0.s
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/dspi_init_c.obj: ../Sources/HAL\ Drivers/dspi_init.c
	@echo 'Building file: $<'
	@echo 'Executing target #4 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/dspi_init.args" -o "Sources/HAL Drivers/dspi_init_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/dspi_init_c.d: ../Sources/HAL\ Drivers/dspi_init.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/dspi_var_c.obj: ../Sources/HAL\ Drivers/dspi_var.c
	@echo 'Building file: $<'
	@echo 'Executing target #5 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/dspi_var.args" -o "Sources/HAL Drivers/dspi_var_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/dspi_var_c.d: ../Sources/HAL\ Drivers/dspi_var.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/emios_init_c.obj: ../Sources/HAL\ Drivers/emios_init.c
	@echo 'Building file: $<'
	@echo 'Executing target #6 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/emios_init.args" -o "Sources/HAL Drivers/emios_init_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/emios_init_c.d: ../Sources/HAL\ Drivers/emios_init.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/excep_handler_s.obj: ../Sources/HAL\ Drivers/excep_handler.s
	@echo 'Building file: $<'
	@echo 'Executing target #7 $<'
	@echo 'Invoking: PowerPC Assembler'
	"$(PAToolsDirEnv)/mwasmeppc" @@"Sources/HAL Drivers/excep_handler.args" -o "Sources/HAL Drivers/excep_handler_s.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/excep_handler_s.d: ../Sources/HAL\ Drivers/excep_handler.s
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/flash_init_c.obj: ../Sources/HAL\ Drivers/flash_init.c
	@echo 'Building file: $<'
	@echo 'Executing target #8 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/flash_init.args" -o "Sources/HAL Drivers/flash_init_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/flash_init_c.d: ../Sources/HAL\ Drivers/flash_init.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/flexcan_init_c.obj: ../Sources/HAL\ Drivers/flexcan_init.c
	@echo 'Building file: $<'
	@echo 'Executing target #9 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/flexcan_init.args" -o "Sources/HAL Drivers/flexcan_init_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/flexcan_init_c.d: ../Sources/HAL\ Drivers/flexcan_init.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/intc_init_c.obj: ../Sources/HAL\ Drivers/intc_init.c
	@echo 'Building file: $<'
	@echo 'Executing target #10 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/intc_init.args" -o "Sources/HAL Drivers/intc_init_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/intc_init_c.d: ../Sources/HAL\ Drivers/intc_init.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/intc_pit_c.obj: ../Sources/HAL\ Drivers/intc_pit.c
	@echo 'Building file: $<'
	@echo 'Executing target #11 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/intc_pit.args" -o "Sources/HAL Drivers/intc_pit_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/intc_pit_c.d: ../Sources/HAL\ Drivers/intc_pit.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/intc_stm_c.obj: ../Sources/HAL\ Drivers/intc_stm.c
	@echo 'Building file: $<'
	@echo 'Executing target #12 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/intc_stm.args" -o "Sources/HAL Drivers/intc_stm_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/intc_stm_c.d: ../Sources/HAL\ Drivers/intc_stm.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/intc_sw_vecttable_c.obj: ../Sources/HAL\ Drivers/intc_sw_vecttable.c
	@echo 'Building file: $<'
	@echo 'Executing target #13 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/intc_sw_vecttable.args" -o "Sources/HAL Drivers/intc_sw_vecttable_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/intc_sw_vecttable_c.d: ../Sources/HAL\ Drivers/intc_sw_vecttable.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/main_c.obj: ../Sources/HAL\ Drivers/main.c
	@echo 'Building file: $<'
	@echo 'Executing target #14 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/main.args" -o "Sources/HAL Drivers/main_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/main_c.d: ../Sources/HAL\ Drivers/main.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/msr_init_c.obj: ../Sources/HAL\ Drivers/msr_init.c
	@echo 'Building file: $<'
	@echo 'Executing target #15 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/msr_init.args" -o "Sources/HAL Drivers/msr_init_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/msr_init_c.d: ../Sources/HAL\ Drivers/msr_init.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/pcu_init_c.obj: ../Sources/HAL\ Drivers/pcu_init.c
	@echo 'Building file: $<'
	@echo 'Executing target #16 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/pcu_init.args" -o "Sources/HAL Drivers/pcu_init_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/pcu_init_c.d: ../Sources/HAL\ Drivers/pcu_init.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/pit_init_c.obj: ../Sources/HAL\ Drivers/pit_init.c
	@echo 'Building file: $<'
	@echo 'Executing target #17 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/pit_init.args" -o "Sources/HAL Drivers/pit_init_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/pit_init_c.d: ../Sources/HAL\ Drivers/pit_init.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/rappid_func_s.obj: ../Sources/HAL\ Drivers/rappid_func.s
	@echo 'Building file: $<'
	@echo 'Executing target #18 $<'
	@echo 'Invoking: PowerPC Assembler'
	"$(PAToolsDirEnv)/mwasmeppc" @@"Sources/HAL Drivers/rappid_func.args" -o "Sources/HAL Drivers/rappid_func_s.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/rappid_func_s.d: ../Sources/HAL\ Drivers/rappid_func.s
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/rappid_utils_c.obj: ../Sources/HAL\ Drivers/rappid_utils.c
	@echo 'Building file: $<'
	@echo 'Executing target #19 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/rappid_utils.args" -o "Sources/HAL Drivers/rappid_utils_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/rappid_utils_c.d: ../Sources/HAL\ Drivers/rappid_utils.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/rchw_init_c.obj: ../Sources/HAL\ Drivers/rchw_init.c
	@echo 'Building file: $<'
	@echo 'Executing target #20 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/rchw_init.args" -o "Sources/HAL Drivers/rchw_init_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/rchw_init_c.d: ../Sources/HAL\ Drivers/rchw_init.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/rgm_init_c.obj: ../Sources/HAL\ Drivers/rgm_init.c
	@echo 'Building file: $<'
	@echo 'Executing target #21 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/rgm_init.args" -o "Sources/HAL Drivers/rgm_init_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/rgm_init_c.d: ../Sources/HAL\ Drivers/rgm_init.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/romcopy_c.obj: ../Sources/HAL\ Drivers/romcopy.c
	@echo 'Building file: $<'
	@echo 'Executing target #22 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/romcopy.args" -o "Sources/HAL Drivers/romcopy_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/romcopy_c.d: ../Sources/HAL\ Drivers/romcopy.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/rtc_init_c.obj: ../Sources/HAL\ Drivers/rtc_init.c
	@echo 'Building file: $<'
	@echo 'Executing target #23 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/rtc_init.args" -o "Sources/HAL Drivers/rtc_init_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/rtc_init_c.d: ../Sources/HAL\ Drivers/rtc_init.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/siu_init_c.obj: ../Sources/HAL\ Drivers/siu_init.c
	@echo 'Building file: $<'
	@echo 'Executing target #24 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/siu_init.args" -o "Sources/HAL Drivers/siu_init_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/siu_init_c.d: ../Sources/HAL\ Drivers/siu_init.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/stm_init_c.obj: ../Sources/HAL\ Drivers/stm_init.c
	@echo 'Building file: $<'
	@echo 'Executing target #25 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/stm_init.args" -o "Sources/HAL Drivers/stm_init_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/stm_init_c.d: ../Sources/HAL\ Drivers/stm_init.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/sw_handlers_s.obj: ../Sources/HAL\ Drivers/sw_handlers.s
	@echo 'Building file: $<'
	@echo 'Executing target #26 $<'
	@echo 'Invoking: PowerPC Assembler'
	"$(PAToolsDirEnv)/mwasmeppc" @@"Sources/HAL Drivers/sw_handlers.args" -o "Sources/HAL Drivers/sw_handlers_s.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/sw_handlers_s.d: ../Sources/HAL\ Drivers/sw_handlers.s
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/swt_init_c.obj: ../Sources/HAL\ Drivers/swt_init.c
	@echo 'Building file: $<'
	@echo 'Executing target #27 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/swt_init.args" -o "Sources/HAL Drivers/swt_init_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/swt_init_c.d: ../Sources/HAL\ Drivers/swt_init.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/sys_init_c.obj: ../Sources/HAL\ Drivers/sys_init.c
	@echo 'Building file: $<'
	@echo 'Executing target #28 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/sys_init.args" -o "Sources/HAL Drivers/sys_init_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/sys_init_c.d: ../Sources/HAL\ Drivers/sys_init.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/HAL\ Drivers/sysclk_init_c.obj: ../Sources/HAL\ Drivers/sysclk_init.c
	@echo 'Building file: $<'
	@echo 'Executing target #29 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/HAL Drivers/sysclk_init.args" -o "Sources/HAL Drivers/sysclk_init_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/HAL\ Drivers/sysclk_init_c.d: ../Sources/HAL\ Drivers/sysclk_init.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '


