################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/Application/Hmi_swc.c" \
"../Sources/Application/Odo_swc.c" \
"../Sources/Application/Speedo_swc.c" \

C_SRCS += \
../Sources/Application/Hmi_swc.c \
../Sources/Application/Odo_swc.c \
../Sources/Application/Speedo_swc.c \

OBJS += \
./Sources/Application/Hmi_swc_c.obj \
./Sources/Application/Odo_swc_c.obj \
./Sources/Application/Speedo_swc_c.obj \

OBJS_QUOTED += \
"./Sources/Application/Hmi_swc_c.obj" \
"./Sources/Application/Odo_swc_c.obj" \
"./Sources/Application/Speedo_swc_c.obj" \

C_DEPS += \
./Sources/Application/Hmi_swc_c.d \
./Sources/Application/Odo_swc_c.d \
./Sources/Application/Speedo_swc_c.d \

OBJS_OS_FORMAT += \
./Sources/Application/Hmi_swc_c.obj \
./Sources/Application/Odo_swc_c.obj \
./Sources/Application/Speedo_swc_c.obj \

C_DEPS_QUOTED += \
"./Sources/Application/Hmi_swc_c.d" \
"./Sources/Application/Odo_swc_c.d" \
"./Sources/Application/Speedo_swc_c.d" \


# Each subdirectory must supply rules for building sources it contributes
Sources/Application/Hmi_swc_c.obj: ../Sources/Application/Hmi_swc.c
	@echo 'Building file: $<'
	@echo 'Executing target #31 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/Application/Hmi_swc.args" -o "Sources/Application/Hmi_swc_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/Application/%.d: ../Sources/Application/%.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '

Sources/Application/Odo_swc_c.obj: ../Sources/Application/Odo_swc.c
	@echo 'Building file: $<'
	@echo 'Executing target #32 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/Application/Odo_swc.args" -o "Sources/Application/Odo_swc_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/Application/Speedo_swc_c.obj: ../Sources/Application/Speedo_swc.c
	@echo 'Building file: $<'
	@echo 'Executing target #33 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/Application/Speedo_swc.args" -o "Sources/Application/Speedo_swc_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '


