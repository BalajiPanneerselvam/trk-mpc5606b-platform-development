################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/Handlers/Clockhandler_swc.c" \

C_SRCS += \
../Sources/Handlers/Clockhandler_swc.c \

OBJS += \
./Sources/Handlers/Clockhandler_swc_c.obj \

OBJS_QUOTED += \
"./Sources/Handlers/Clockhandler_swc_c.obj" \

C_DEPS += \
./Sources/Handlers/Clockhandler_swc_c.d \

OBJS_OS_FORMAT += \
./Sources/Handlers/Clockhandler_swc_c.obj \

C_DEPS_QUOTED += \
"./Sources/Handlers/Clockhandler_swc_c.d" \


# Each subdirectory must supply rules for building sources it contributes
Sources/Handlers/Clockhandler_swc_c.obj: ../Sources/Handlers/Clockhandler_swc.c
	@echo 'Building file: $<'
	@echo 'Executing target #1 $<'
	@echo 'Invoking: PowerPC Compiler'
	"$(PAToolsDirEnv)/mwcceppc" @@"Sources/Handlers/Clockhandler_swc.args" -o "Sources/Handlers/Clockhandler_swc_c.obj" "$<" -MD -gccdep
	@echo 'Finished building: $<'
	@echo ' '

Sources/Handlers/%.d: ../Sources/Handlers/%.c
	@echo 'Regenerating dependency file: $@'
	
	@echo ' '


